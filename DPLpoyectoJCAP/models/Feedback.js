// Constructor para el modelo con atributos
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

//Esquema de la base de datos
let feedbackSchema = new Schema({
  feedbackID: Number,
  name: String,
  calificacion: String,
  critica: String,
  fecha: Date
});

// ------ Métodos ------ //
//Show Feedback
feedbackSchema.statics.allFeedback = function(cb) {
  return this.find({}, cb);
};

//Create Feedback
feedbackSchema.statics.add = function(aFeedback, cb) {
  return this.create(aFeedback, cb);
};

//Find By Id Feedback
feedbackSchema.statics.findById = function(anId, cb) {
  return this.findOne(
    {
      _id: anId
    },
    cb
  );
};

//Delete By Id Feedback
feedbackSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne(
    {
      _id: anId
    },
    cb
  );
};

//Update By Id Feedback
feedbackSchema.statics.findAndUpdate = function(anId, data, cb) {
  return this.findByIdAndUpdate(
    {
      _id: anId
    },
    data,
    cb
  );
};

module.exports = mongoose.model("Feedback", feedbackSchema);
