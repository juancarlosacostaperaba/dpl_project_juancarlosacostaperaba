// Constructor para el modelo con atributos
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

//Esquema de la base de datos
let promotionsSchema = new Schema({
  promotionID: Number,
  name: String,
  image: String,
  label: String,
  price: String,
  featured: Boolean,
  description: String
});

// ------ Métodos ------ //
//Show Promotions
promotionsSchema.statics.allPromotions = function(cb) {
  return this.find({}, cb);
};

//Create Promotions
promotionsSchema.statics.add = function(aPromotions, cb) {
  return this.create(aPromotions, cb);
};

//Find By Id Promotions
promotionsSchema.statics.findById = function(anId, cb) {
  return this.findOne(
    {
      _id: anId
    },
    cb
  );
};

//Delete By Id Promotions
promotionsSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne(
    {
      _id: anId
    },
    cb
  );
};

//Update By Id Promotions
promotionsSchema.statics.findAndUpdate = function(anId, data, cb) {
  return this.findByIdAndUpdate(
    {
      _id: anId
    },
    data,
    cb
  );
};

module.exports = mongoose.model("Promotions", promotionsSchema);
