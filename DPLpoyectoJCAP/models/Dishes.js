// Constructor para el modelo con atributos
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

//Esquema de la base de datos
let dishesSchema = new Schema({
  dishID: Number,
  name: String,
  image: String,
  category: String,
  label: String,
  price: String,
  featured: Boolean,
  description: String,
  comments: {
    type: [
      {
        rating: Number,
        comment: String,
        author: String,
        date: Date
      }
    ],
    index: true
  }
});

// ------ Métodos ------ //
//Show Dishes
dishesSchema.statics.allDishes = function(cb) {
  return this.find({}, cb);
};

//Create Dishes
dishesSchema.statics.add = function(aDish, cb) {
  return this.create(aDish, cb);
};

//Find By Id Dishes
dishesSchema.statics.findById = function(anId, cb) {
  return this.findOne(
    {
      _id: anId
    },
    cb
  );
};

//Delete By Id Dishes
dishesSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne(
    {
      _id: anId
    },
    cb
  );
};

//Update By Id Dishes
dishesSchema.statics.findAndUpdate = function(anId, data, cb) {
  return this.findByIdAndUpdate(
    {
      _id: anId
    },
    data,
    cb
  );
};

//Update Comments
dishesSchema.statics.findAndUpdateComments = function(anId, coId, data, cb) {
  return this.findOneAndUpdate(
    {
      _id: anId,
      "comments._id": coId
    },
    data,
    cb
  );
};

module.exports = mongoose.model("Dishes", dishesSchema);
