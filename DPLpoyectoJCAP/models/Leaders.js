// Constructor para el modelo con atributos
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

//Esquema de la base de datos
let leadersSchema = new Schema({
  leaderID: Number,
  name: String,
  image: String,
  designation: String,
  abbr: String,
  featured: Boolean,
  description: String
});

// ------ Métodos ------ //
//Show Leaders
leadersSchema.statics.allLeaders = function(cb) {
  return this.find({}, cb);
};

//Create Leaders
leadersSchema.statics.add = function(aLeaders, cb) {
  return this.create(aLeaders, cb);
};

//Find By Id Leaders
leadersSchema.statics.findById = function(anId, cb) {
  return this.findOne(
    {
      _id: anId
    },
    cb
  );
};

//Delete By Id Leaders
leadersSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne(
    {
      _id: anId
    },
    cb
  );
};

//Update By Id Leaders
leadersSchema.statics.findAndUpdate = function(anId, data, cb) {
  return this.findByIdAndUpdate(
    {
      _id: anId
    },
    data,
    cb
  );
};

module.exports = mongoose.model("Leaders", leadersSchema);
