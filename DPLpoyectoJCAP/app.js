//=============== IMPORTS ===============//
//Errores
var createError = require("http-errors");
//Express
var express = require("express");
//Favicon
var favicon = require("serve-favicon");
//Path
var path = require("path");
//Cookies
var cookieParser = require("cookie-parser");
//Logger
var logger = require("morgan");
//Mongoose
var mongoose = require("mongoose");
//App
var app = express();
//Routes
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
//Dishes API
var dishesAPIRouter = require("./routes/api/dishes");
//Promotions API
var promotionsAPIRouter = require("./routes/api/promotions");
//Leaders API
var leadersAPIRouter = require("./routes/api/leaders");
//Feedback API
var feedbackAPIRouter = require("./routes/api/feedback");

// ------ DATABASE ------- //
mongoose.connect('mongodb://localhost/restaurante', { useUnifiedTopology: true, useNewUrlParser: true  });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
//Bind connection to error event (to get notification of connection errors)
db.on("error", console.error.bind('Error de conexión con MongoDB'));

//=============== VIEW ENGINE SETUP ===============//
//Configuración
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
//Cookies
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

//=============== API ===============//
//Dishes API
app.use("/api/dishes", dishesAPIRouter);
//Promotons API
app.use("/api/promotions", promotionsAPIRouter);
//Leaders API
app.use("/api/leaders", leadersAPIRouter);
//Feedback API
app.use("/api/feedback", feedbackAPIRouter);

//=============== ERRORES ===============//
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
