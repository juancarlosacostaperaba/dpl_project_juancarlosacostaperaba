let Feedback = require("../../models/Feedback");

//Show Feedback
exports.feedback_list = function(req, res) {
  Feedback.allFeedback(function(err, feedback) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(feedback);
  });
};

//Create Feedback
exports.feedback_create = function(req, res) {
  Feedback.add(
    {
      feedbackID: req.body.feedbackID,
      name: req.body.name,
      calificacion: req.body.calificacion,
      critica: req.body.critica,
      fecha: req.body.fecha
    },
    function(err, feedback) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(feedback);
    }
  );
};

//Delete By Id Feedback
exports.feedback_delete = function(req, res) {
  Feedback.removeById(req.body._id, function(err, feedback) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(feedback);
  });
};

//Update By Id Feedback
exports.feedback_update = function(req, res) {
  Feedback.findAndUpdate(
    req.params._id,
    {
      feedbackID: req.body.feedbackID,
      name: req.body.name,
      calificacion: req.body.calificacion,
      critica: req.body.critica,
      fecha: req.body.fecha
    },
    function(err, feedback) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(feedback);
    }
  );
};
