let Dishes = require("../../models/Dishes");

//Show Dishes
exports.dish_list = function(req, res) {
  Dishes.allDishes(function(err, dishes) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(dishes);
  });
};

//Create Dishes
exports.dish_create = function(req, res) {
  Dishes.add(
    {
      dishID: req.body.dishID,
      name: req.body.name,
      image: req.body.image,
      category: req.body.category,
      label: req.body.label,
      price: req.body.price,
      featured: req.body.featured,
      description: req.body.description,
      comments: []
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};

//Delete By Id Dishes
exports.dish_delete = function(req, res) {
  Dishes.removeById(req.body._id, function(err, dish) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(dish);
  });
};

//Update By Id Dishes
exports.dish_update = function(req, res) {
  Dishes.findAndUpdate(
    req.params._id,
    {
      dishID: req.body.dishID,
      name: req.body.name,
      image: req.body.image,
      category: req.body.category,
      label: req.body.label,
      price: req.body.price,
      featured: req.body.featured,
      description: req.body.description
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};

//Create Comments
exports.dish_comment = function(req, res) {
  Dishes.findAndUpdate(
    req.params._id,
    {
      $push: {
        comments: [
          {
            rating: req.body.rating,
            comment: req.body.comment,
            author: req.body.author,
            date: req.body.date
          }
        ]
      }
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};

//Delete Comments
exports.dish_deleteComment = function(req, res) {
  Dishes.findAndUpdate(
    req.params._id,
    {
      $pull: {
        comments: {
          _id: [req.body.comment_id]
        }
      }
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};

//Update By Id Comments
exports.dish_updateComment = function(req, res) {
  Dishes.findAndUpdateComments(
    req.params._id,
    req.body.comment_id,
    {
      $set: {
        "comments.$.rating": req.body.rating,
        "comments.$.comment": req.body.comment,
        "comments.$.author": req.body.author,
        "comments.$.date": req.body.date
      }
    },
    function(err, dish) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(dish);
    }
  );
};
