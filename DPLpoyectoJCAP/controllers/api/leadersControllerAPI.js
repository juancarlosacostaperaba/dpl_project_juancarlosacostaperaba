let Leaders = require("../../models/Leaders");

//Show Leaders
exports.leader_list = function(req, res) {
  Leaders.allLeaders(function(err, leaders) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(leaders);
  });
};

//Create Leaders
exports.leader_create = function(req, res) {
  Leaders.add(
    {
      leaderID: req.body.leaderID,
      name: req.body.name,
      image: req.body.image,
      designation: req.body.designation,
      abbr: req.body.abbr,
      featured: req.body.featured,
      description: req.body.description
    },
    function(err, leader) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(leader);
    }
  );
};

//Delete By Id Leaders
exports.leader_delete = function(req, res) {
  Leaders.removeById(req.body._id, function(err, leader) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(leader);
  });
};

//Update By Id Leaders
exports.leader_update = function(req, res) {
  Leaders.findAndUpdate(
    req.params._id,
    {
      leaderID: req.body.leaderID,
      name: req.body.name,
      image: req.body.image,
      designation: req.body.designation,
      abbr: req.body.abbr,
      featured: req.body.featured,
      description: req.body.description
    },
    function(err, leader) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(leader);
    }
  );
};
