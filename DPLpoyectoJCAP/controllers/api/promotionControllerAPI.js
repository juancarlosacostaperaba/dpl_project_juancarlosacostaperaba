let Promotions = require("../../models/Promotions");

//Show Promotions
exports.promotion_list = function(req, res) {
  Promotions.allPromotions(function(err, promotions) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(promotions);
  });
};

//Create Promotions
exports.promotion_create = function(req, res) {
  Promotions.add(
    {
      promotionID: req.body.promotionID,
      name: req.body.name,
      image: req.body.image,
      label: req.body.label,
      price: req.body.price,
      featured: req.body.featured,
      description: req.body.description
    },
    function(err, promotion) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(promotion);
    }
  );
};

//Delete By Id Promotions
exports.promotion_delete = function(req, res) {
  Promotions.removeById(req.body._id, function(err, promotion) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(promotion);
  });
};

//Update By Id Promotions
exports.promotion_update = function(req, res) {
  Promotions.findAndUpdate(
    req.params._id,
    {
      promotionID: req.body.promotionID,
      name: req.body.name,
      image: req.body.image,
      label: req.body.label,
      price: req.body.price,
      featured: req.body.featured,
      description: req.body.description
    },
    function(err, promotion) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(promotion);
    }
  );
};
