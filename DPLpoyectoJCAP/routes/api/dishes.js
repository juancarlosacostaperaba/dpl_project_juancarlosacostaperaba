let express = require("express");
let router = express.Router();
let dishControllerAPI = require("../../controllers/api/dishControllerAPI");

// -------- DISHES -------- //
//Show Dishes
router.get("/", dishControllerAPI.dish_list);

//Create Dishes
router.post("/create", dishControllerAPI.dish_create);

//Delete Dishes
router.delete("/delete", dishControllerAPI.dish_delete);

//Update Dishes
router.put("/:_id/update", dishControllerAPI.dish_update);

// -------- COMMENTS -------- //
//Create Comments
router.post("/:_id/comment", dishControllerAPI.dish_comment);

//Delete Comments
router.delete("/:_id/deleteComment", dishControllerAPI.dish_deleteComment);

//Update Comments
router.put("/:_id/updateComment", dishControllerAPI.dish_updateComment);

module.exports = router;
