let express = require("express");
let router = express.Router();
let promotionControllerAPI = require("../../controllers/api/promotionControllerAPI");

// -------- PROMOTIONS -------- //
//Show Promotions
router.get("/", promotionControllerAPI.promotion_list);

//Create Promotions
router.post("/create", promotionControllerAPI.promotion_create);

//Delete Promotions
router.delete("/delete", promotionControllerAPI.promotion_delete);

//Update Promotions
router.put("/:_id/update", promotionControllerAPI.promotion_update);

module.exports = router;
