let express = require("express");
let router = express.Router();
let feedbackControllerAPI = require("../../controllers/api/feedbackControllerAPI");

// -------- FEEDBACK -------- //
//Show Feedback
router.get("/", feedbackControllerAPI.feedback_list);

//Create Feedback
router.post("/create", feedbackControllerAPI.feedback_create);

//Delete Feedback
router.delete("/delete", feedbackControllerAPI.feedback_delete);

//Update Feedback
router.put("/:_id/update", feedbackControllerAPI.feedback_update);

module.exports = router;
