let express = require("express");
let router = express.Router();
let leadersControllerAPI = require("../../controllers/api/leadersControllerAPI");

// -------- LEADERS -------- //
//Show Leaders
router.get("/", leadersControllerAPI.leader_list);

//Create Leaders
router.post("/create", leadersControllerAPI.leader_create);

//Delete Leaders
router.delete("/delete", leadersControllerAPI.leader_delete);

//Update Leaders
router.put("/:_id/update", leadersControllerAPI.leader_update);

module.exports = router;
